# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(nome:"Maria", sobrenome: "Silva", avatar: "MS", email: "mariasilva@mail.com", cidade: "São Paulo", perfil: "Admin", password:"123456", password_confirmation: "123456")
User.create(nome:"José", sobrenome: "Santos", avatar: "JS", email: "josesantos@mail.com", cidade: "São Paulo", perfil: "Cliente", password:"123456", password_confirmation: "123456")
