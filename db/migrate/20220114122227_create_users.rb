class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :nome
      t.string :sobrenome
      t.string :avatar
      t.string :email
      t.string :cidade
      t.string :perfil

      t.timestamps
    end
  end
end
