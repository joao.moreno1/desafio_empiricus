# Desafio Empiricus

### Recursos utilizados:



- Ruby versão  2.7.4p191
- Rails versão 6.1.4.4
- Banco de dados PostgreSQL versão 13

Caso queira clonar o projeto e instalar localmente, siga o passo número 1, senão siga o passo 2 para testá-lo diretamente pela plataforma Heroku.

### 1 - Instalando o projeto:


Acessar o Gitlab: https://gitlab.com/joao.moreno1/desafio_empiricus

Clonar o projeto.

Instalar as gems rodando: `bundle install`

Em database.yml - alterar o usuário e senha do banco

Rodar os seguintes comandos para criar o banco e alimentar com dois usuários:
```
rails db:create
rails db:migrate
rails db:seed
```
### 2 - Acessando o projeto Heroku:


O projeto encontra-se disponível na plataforma Heroku e não possui interface gráfica, segue abaixo o link para acesso.
Observação: A primeira requisição pode demorar alguns segundos, ou até falhar, tendo em vista o projeto utilizar um plano gratuito que pode deixá-lo em espera.

https://murmuring-earth-92141.herokuapp.com


- Gems (bibliotecas) utilizadas:

	- devise (para autenticação do usuário)
	- jwt (assinatura criptográfica - token)


- No banco de dados estão cadastrados dois usuários, sendo um "Admin" e outro "Cliente"	

Utilizando o Imnsonia:

## Usuário Admin

### Fazer login:

```
POST  https://murmuring-earth-92141.herokuapp.com/users/sign_in

JSON:
{
	"user": {
		"email": "mariasilva@mail.com",
		"password": "123456"
	}
}

Nos Headers da resposta, copiar Authorization

(exemplo)	Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzIiwic2NwIjoidXNlciIsImF1ZCI6bnVsbCwiaWF0IjoxNjQ4OTg1ODU0LCJleHAiOjE2NDg5ODk0NTQsImp0aSI6IjJlNTU4OTA3LWMzOWEtNDYzNi04N2RlLTEzZjQwYjhmZmI3MyJ9.WffroyhiumJAPVsio5FZybZCDZZpyhbFlYpMO1O_J34
```

### Criar usuário:
```
POST  https://murmuring-earth-92141.herokuapp.com/users

No Header da requisição, criar:
New header: Authorization
New value: Colar o token fornecido no primeiro acesso.
Exemplo: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzIiwic2NwIjoidXNlciIsImF1ZCI6bnVsbCwiaWF0IjoxNjQ4OTg1ODU0LCJleHAiOjE2NDg5ODk0NTQsImp0aSI6IjJlNTU4OTA3LWMzOWEtNDYzNi04N2RlLTEzZjQwYjhmZmI3MyJ9.WffroyhiumJAPVsio5FZybZCDZZpyhbFlYpMO1O_J34

{
	"user":{
		"nome": "Carlos",
		"sobrenome": "Martins",
		"avatar": "CM",
		"email": "carlosmartins@mail.com",
		"cidade": "São Paulo",
		"perfil": "Cliente",
		"password": "123456",
		"password_confirmation": "123456"	
	}
}
Exemplo do resultado esperado:
{
	"id": 8,
	"nome": "Carlos",
	"sobrenome": "Martins",
	"avatar": "CM",
	"email": "carlosmartins@mail.com",
	"cidade": "São Paulo",
	"perfil": "Cliente",
	"created_at": "2022-04-03T11:54:14.354Z",
	"updated_at": "2022-04-03T11:54:14.354Z"
}
```

### Mostrar apenas um usuário:
```
GET  https://murmuring-earth-92141.herokuapp.com/users/:id

No Header da requisição, criar:
New header: Authorization
New value: Colar o token fornecido no primeiro acesso.
Exemplo: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzIiwic2NwIjoidXNlciIsImF1ZCI6bnVsbCwiaWF0IjoxNjQ4OTg1ODU0LCJleHAiOjE2NDg5ODk0NTQsImp0aSI6IjJlNTU4OTA3LWMzOWEtNDYzNi04N2RlLTEzZjQwYjhmZmI3MyJ9.WffroyhiumJAPVsio5FZybZCDZZpyhbFlYpMO1O_J34

Exemplo do resultado esperado:
{
	"id": 3,
	"nome": "Maria",
	"sobrenome": "Silva",
	"avatar": "MS",
	"email": "mariasilva@mail.com",
	"cidade": "São Paulo",
	"perfil": "Admin",
	"created_at": "2022-04-02T11:27:42.585Z",
	"updated_at": "2022-04-02T11:27:42.585Z"
}
```


### Listar usuários:
```
GET  https://murmuring-earth-92141.herokuapp.com/users

No Header da requisição, criar:
New header: Authorization
New value: Colar o token fornecido no primeiro acesso.
Exemplo: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzIiwic2NwIjoidXNlciIsImF1ZCI6bnVsbCwiaWF0IjoxNjQ4OTg1ODU0LCJleHAiOjE2NDg5ODk0NTQsImp0aSI6IjJlNTU4OTA3LWMzOWEtNDYzNi04N2RlLTEzZjQwYjhmZmI3MyJ9.WffroyhiumJAPVsio5FZybZCDZZpyhbFlYpMO1O_J34
 

Exemplo do resultado esperado:

[
	{
		"id": 3,
		"nome": "Maria",
		"sobrenome": "Silva",
		"avatar": "MS",
		"email": "mariasilva@mail.com",
		"cidade": "São Paulo",
		"perfil": "Admin",
		"created_at": "2022-04-02T11:27:42.585Z",
		"updated_at": "2022-04-02T11:27:42.585Z"
	},
	{
		"id": 4,
		"nome": "José",
		"sobrenome": "Santos",
		"avatar": "JS",
		"email": "josesantos@mail.com",
		"cidade": "São Paulo",
		"perfil": "Cliente",
		"created_at": "2022-04-02T11:27:43.809Z",
		"updated_at": "2022-04-02T11:27:43.809Z"
	},
	{
		"id": 8,
		"nome": "Carlos",
		"sobrenome": "Martins",
		"avatar": "CM",
		"email": "carlosmartins@mail.com",
		"cidade": "São Paulo",
		"perfil": "Cliente",
		"created_at": "2022-04-03T11:54:14.354Z",
		"updated_at": "2022-04-03T11:54:14.354Z"
	}
]
```
### Excluir usuário:
```
GET  https://murmuring-earth-92141.herokuapp.com/users/:id

No Header da requisição, criar
New header: Authorization
New value: Colar o token fornecido no primeiro acesso.
Exemplo: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzIiwic2NwIjoidXNlciIsImF1ZCI6bnVsbCwiaWF0IjoxNjQ4OTg1ODU0LCJleHAiOjE2NDg5ODk0NTQsImp0aSI6IjJlNTU4OTA3LWMzOWEtNDYzNi04N2RlLTEzZjQwYjhmZmI3MyJ9.WffroyhiumJAPVsio5FZybZCDZZpyhbFlYpMO1O_J34


Exemplo do resultado esperado:

204 No Content
```
 

### Atualizar usuário:
```
PUT  https://murmuring-earth-92141.herokuapp.com/users/:id

No Header da requisição, criar
New header: Authorization
New value: Colar o token fornecido no primeiro acesso.
Exemplo: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzIiwic2NwIjoidXNlciIsImF1ZCI6bnVsbCwiaWF0IjoxNjQ4OTg1ODU0LCJleHAiOjE2NDg5ODk0NTQsImp0aSI6IjJlNTU4OTA3LWMzOWEtNDYzNi04N2RlLTEzZjQwYjhmZmI3MyJ9.WffroyhiumJAPVsio5FZybZCDZZpyhbFlYpMO1O_J34


	"user":{
		"nome": "Carlos",
		"sobrenome": "Martins",
		"avatar": "CM",
		"email": "carlosmartins@mail.com",
		"cidade": "São Paulo",
		"perfil": "Admin",
		"password": "123456",
		"password_confirmation": "123456"	
	}
}

Exemplo do resultado esperado:


{
	"nome": "Carlos",
	"sobrenome": "Martins",
	"avatar": "CM",
	"email": "carlosmartins@mail.com",
	"cidade": "São Paulo",
	"perfil": "Admin",
	"id": 8,
	"created_at": "2022-04-03T11:54:14.354Z",
	"updated_at": "2022-04-03T12:00:13.944Z"
}
```
## Usuário Cliente

Acessando como usuário "Cliente", a resposta conterá um token, assim como para "Admin". Dentre as ações acima descritas ele poderá acessar  "Mostrar apenas um usuário" e ver apenas seu próprio perfil.

