class ApplicationController < ActionController::API
    respond_to :json
    include ActionController::MimeResponds

    AcessoNegado = Class.new(StandardError)
    rescue_from AcessoNegado do |exception|
        render json: { mensagem: exception.message }, status: :forbidden
    end
end
