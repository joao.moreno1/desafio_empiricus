class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable, :jwt_authenticatable, jwt_revocation_strategy: JwtDenylist
    #Somente serão registrados usuários com no mínimo nome, sobrenome e email
    #validates :nome, :sobrenome, :email, presence: true

    
end
